package tn.esprit.kaddem.Services;

import tn.esprit.kaddem.Entities.*;

import java.util.List;

public interface IEtudiantService {


    List<Etudiant> retrieveAllEtudiants();
    Etudiant addEtudiant (Etudiant e);
    Etudiant updateEtudiant (Etudiant e);
    Etudiant retrieveEtudiant(long idEtudiant);
    void removeEtudiant(long idEtudiant);

    void assignEtudiantToDepartement (long etudiantId, long departementId);

    Etudiant addAndAssignEtudiantToEquipeAndContract(Etudiant e, long idContrat, long idEquipe);

    List<Etudiant> getEtudiantsByDepartement (int idDepartement);
}


