package tn.esprit.kaddem.Services;


import tn.esprit.kaddem.Entities.DetailsEquipe;

import java.util.List;

public interface IDetailsEquipeService {
    List<DetailsEquipe> selectBySalle(int salle);
    DetailsEquipe addDetaisEquipe(DetailsEquipe DE);
    DetailsEquipe updateDetailsEquipe (DetailsEquipe DE);
    List<DetailsEquipe> retrieveAllDetailsEquipe ();
}