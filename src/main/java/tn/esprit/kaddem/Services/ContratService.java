package tn.esprit.kaddem.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.kaddem.Entities.Contrat;
import tn.esprit.kaddem.Entities.Etudiant;
import tn.esprit.kaddem.Repositories.ContratRepository;
import tn.esprit.kaddem.Repositories.EtudiantRepository;

import java.util.Date;
import java.util.List;

@Service
public class ContratService implements IContratService{
    @Autowired // @Inject  =new ContratRepository
    private ContratRepository cr;
    @Autowired
    private EtudiantRepository er;

    @Override
    public Contrat addContrat(Contrat c) {
        return cr.save(c);
    }

    @Override
    public List<Contrat> getAllContrat() {
        return (List<Contrat>) cr.findAll();
    }

    @Override
    public Contrat updateContrat(Contrat c) {
        return cr.save(c);
    }

    @Override
    public Contrat getContratById(int id) {

        return cr.findById(id).get();

    }

    @Override
    public void deleteContratById(int id) {
        cr.deleteById(id);
    }

    @Override
    public void deleteContrat(Contrat c) {
        cr.delete(c);

    }

    @Override
    public Contrat affectContratToEtudiant(Contrat ce, String nomE, String prenomE) {
       return null;
    }

    @Override
    public Integer nbContratsValide(Date startDate, Date endDate) {
        return cr.nbContratsValide(startDate,endDate);
    }

    @Override
    public String retrieveAndUpdateStatusContrat() {
        return null;
    }
}
