package tn.esprit.kaddem.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.kaddem.Repositories.*;
import tn.esprit.kaddem.Entities.*;
import java.util.List;

@Service
public class EtudiantService implements IEtudiantService {
    @Override
    public List<Etudiant> retrieveAllEtudiants() {
        return null;
    }

    @Override
    public Etudiant addEtudiant(Etudiant e) {
        return null;
    }

    @Override
    public Etudiant updateEtudiant(Etudiant e) {
        return null;
    }

    @Override
    public Etudiant retrieveEtudiant(long idEtudiant) {
        return null;
    }

    @Override
    public void removeEtudiant(long idEtudiant) {

    }

    @Override
    public void assignEtudiantToDepartement(long etudiantId, long departementId) {

    }

    @Override
    public Etudiant addAndAssignEtudiantToEquipeAndContract(Etudiant e, long idContrat, long idEquipe) {
        return null;
    }

    @Override
    public List<Etudiant> getEtudiantsByDepartement(int idDepartement) {
        return null;
    }
/*
    @Autowired
    EtudiantRepository er;

    @Autowired
    DepartementRepository dr;

    @Autowired
    ContratRepository cr;
    @Autowired
    EquipeRepository  eqr;


    @Override
    public List<Etudiant> retrieveAllEtudiants() {
        return (List<Etudiant>) er.findAll();
    }

    @Override
    public Etudiant addEtudiant(Etudiant e) {
        return er.save(e);
    }

    @Override
    public Etudiant updateEtudiant(Etudiant e) {
        return er.save(e);
    }

    @Override
    public Etudiant retrieveEtudiant(long idEtudiant) {
        return   er.findById(idEtudiant).get() ;
    }

    @Override
    public void removeEtudiant(long idEtudiant) {
        er.deleteById(idEtudiant);


    }



    @Override
    public void assignEtudiantToDepartement(long etudiantId, long departementId) {
        //1-recuperation des objets;
        Etudiant etudiant= er.findById(etudiantId).get();
        Departement departement=dr.findById(departementId).get();
        //2-definir le parent et le child (dans ce cas : parent est l' etudiant/child=departement)
        //3-on affecte le child au parent
        etudiant.setD(departement);
        //4-save to parent
        er.save(etudiant);


    }

    @Override
    /*public Etudiant addAndAssignEtudiantToEquipeAndContract(Etudiant e, long idContrat, long  idEquipe) {

        Contrat contrat = cr.findById(idContrat).get();
        Equipe equipe = eqr.findById(idEquipe).get();

        //1-relation entre contrat : parent/etudiant:child(on ajouté

        contrat.setE(e);
        cr.save(contrat);

        //2-relation entre equipe : child/etudiant:parent
        e.getEquipes().add(equipe);
        return er.save(e);
    }

    @Override
    public List<Etudiant> getEtudiantsByDepartement(int idDepartement) {
        //------method1-------
        //Departement departement=dr.findById(idDepartement).get();
        // return departement.getEtudiants();

        // -----method 2-----
        //return er.getEtudiantByDIdDepartement(idDepartement);

        // -----method 3-----
        // return er.selectEtudiantByDIdDepartementSQL(idDepartement);

        // -----method 4-----
        return er.selectEtudiantByDIdDepartementSQL(idDepartement);
    }
*/


}