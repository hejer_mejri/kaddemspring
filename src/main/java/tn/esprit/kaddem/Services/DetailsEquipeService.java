package tn.esprit.kaddem.Services;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.kaddem.Entities.DetailsEquipe;
import tn.esprit.kaddem.Repositories.DetailsEquipeRepository;

import java.util.List;

@Service
public class DetailsEquipeService implements IDetailsEquipeService{

    @Autowired
    private DetailsEquipeRepository der;
    @Override
    public List<DetailsEquipe> selectBySalle(int salle) {
        return der.findBySalle(salle);
    }

    @Override
    public DetailsEquipe addDetaisEquipe(DetailsEquipe DE) {
        return der.save(DE);
    }

    @Override
    public DetailsEquipe updateDetailsEquipe(DetailsEquipe DE) {
        return der.save(DE);
    }

    @Override
    public List<DetailsEquipe> retrieveAllDetailsEquipe() {
        return (List<DetailsEquipe>) der.findAll();
    }
}
