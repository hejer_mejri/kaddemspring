package tn.esprit.kaddem.Services;

import tn.esprit.kaddem.Entities.Contrat;

import java.util.Date;
import java.util.List;

public interface IContratService {
    Contrat addContrat(Contrat c);
    List<Contrat> getAllContrat();
    Contrat updateContrat (Contrat c);
    Contrat getContratById(int id);
    void deleteContratById(int id);
    void deleteContrat(Contrat c);
    Contrat affectContratToEtudiant (Contrat ce, String nomE ,String prenomE);
    Integer nbContratsValide(Date startDate, Date endDate);
    String retrieveAndUpdateStatusContrat();
}
