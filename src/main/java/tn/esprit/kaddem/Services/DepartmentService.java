package tn.esprit.kaddem.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.kaddem.Entities.Department;
import tn.esprit.kaddem.Entities.Universite;
import tn.esprit.kaddem.Repositories.DepartementRepository;
import tn.esprit.kaddem.Repositories.UniversiteRepository;

import java.util.List;

@Service
public class DepartmentService implements IDepartmentService{
    @Autowired
    private DepartementRepository dr;

    @Autowired
    private UniversiteRepository ur;


    @Override
    public List<Department> retrieveAllDepartements() {
        return (List<Department>) dr.findAll();
    }

    @Override
    public Department addDepartement(Department d) {

        return dr.save(d);
    }

    @Override
    public Department updateDepartement(Department d) {
        return dr.save(d);
    }

    @Override
    public Department retrieveDepartement(long idDepart) {
        return   dr.findById(idDepart).get() ;
    }

    @Override
    public void assignUniversiteToDepartement(long idUniversite, long idDepartement) {

        //1-recuperation des objets
        Department departement = dr.findById(idDepartement).get();
        Universite university = ur.findById(idUniversite).get();
        //2-parent=>universite / child=> department
        //3-affecter child au parent
        university.getDepartements().add(departement);
        //4-save university
        ur.save(university);
    }


    /*@Override
    public List<Departement> retrieveDepartementsByUniversite(int idUniversite) {
         return   dr.findDepartmentByUniversite(idUniversite);
    }*/
}

