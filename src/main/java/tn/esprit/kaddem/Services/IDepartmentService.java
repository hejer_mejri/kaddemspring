package tn.esprit.kaddem.Services;


import tn.esprit.kaddem.Entities.Department;

import java.util.List;

public interface IDepartmentService {
    List<Department> retrieveAllDepartements();
    Department addDepartement (Department d);
    Department updateDepartement (Department d);
    Department retrieveDepartement (long idDepart);

    /* List<Departement> retrieveDepartementsByUniversite(int idUniversite);*/
    void assignUniversiteToDepartement(long idUniversite, long idDepartement);

}


