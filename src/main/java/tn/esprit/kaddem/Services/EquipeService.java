package tn.esprit.kaddem.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.kaddem.Entities.Equipe;
import tn.esprit.kaddem.Repositories.EquipeRepository;

import java.util.List;

@Service

public class EquipeService {
    @Autowired
    private EquipeRepository eqr;

   @Override
    public List<Equipe> retrieveAllEquipes() {
        return (List<Equipe>) eqr.findAll();
    }

    @Override
    public Equipe addEquipe(Equipe e) {
        return eqr.save(e);
    }

    @Override
    public Equipe updateEquipe(Equipe e) {
        return eqr.save(e);    }

    @Override
    public Equipe retrieveEquipe(long id) {
        return   eqr.findById(id).get() ;    }
}
