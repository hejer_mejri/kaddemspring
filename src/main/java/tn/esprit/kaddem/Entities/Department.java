
package tn.esprit.kaddem.Entities;
import lombok.*;
import lombok.experimental.FieldDefaults;
import tn.esprit.kaddem.Entities.Etudiant;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "Departement")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Department implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDepartement")
    Long idDepartement;
    @Column(name = "nomDepartement")
    String nomDepartement;
    @OneToMany(mappedBy = "d")
    List<Etudiant> etudiants;
}
