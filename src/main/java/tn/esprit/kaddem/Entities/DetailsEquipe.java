package tn.esprit.kaddem.Entities;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "DetailsEquipe")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DetailsEquipe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDetailsEquipe")
    Long idDetailsEquipe;
    @Column(name = "Salle")
    int salle;
    @Column(name = "thematique")
    String thematique;

    @JsonIgnore
    @OneToOne(mappedBy = "DetailsEquipe")
    Equipe eq;
}
