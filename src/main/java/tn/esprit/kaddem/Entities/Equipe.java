package tn.esprit.kaddem.Entities;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;



@Entity
@Table(name = "Equipe")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Equipe implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdEquipe")
    Long idEquipe;
    @Column(name = "nomEquipe")
    String nomEquipe;
    @Column(name = "Niveau")
    @Enumerated(EnumType.STRING)
    Niveau niveau;
    @OneToOne(cascade = CascadeType.ALL)
    DetailsEquipe DetailsEquipe;
    @ManyToMany(mappedBy = "equipes")// kif nhotou mappedby yaany hetha e child
    List<Etudiant> etudiants;
}
