package tn.esprit.kaddem.Entities;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;



@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@ToString
@EqualsAndHashCode
public class Etudiant implements Serializable{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idEtudiant;
    String prenomE;
    String nomE;
    @Enumerated(EnumType.STRING)
    Domaine domaine;
    
}
