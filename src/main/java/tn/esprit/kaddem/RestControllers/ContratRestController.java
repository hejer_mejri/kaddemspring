package tn.esprit.kaddem.RestControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import tn.esprit.kaddem.Entities.Contrat;
import tn.esprit.kaddem.Services.IContratService;

import java.util.Date;
import java.util.List;

@RestController
public class ContratRestController {

        @Autowired
        private IContratService ics;
        @GetMapping("/retrieveAllContrat")
        public List<Contrat> retrieveAllContrat(){
            return ics.getAllContrat();
        }
        @PostMapping("/addContrat")
        public Contrat ajouterContrat  (@RequestBody Contrat c){ //les information ijiwek men bara
            return ics.addContrat(c);
        }

        @PutMapping("/updateContrat")
        public Contrat updateContart(@RequestBody  Contrat c){
            return  ics.updateContrat(c);
        }
        @GetMapping("retrieveContrat/{idContrat}")
        public Contrat retrieveContrat(@PathVariable Integer idContrat){
            return  ics.getContratById(idContrat);
        }
        @DeleteMapping("/deleteContrat")
        void removeContrat(@RequestParam Integer idContrat){
            ics.deleteContratById(idContrat);
        }

        @GetMapping("nbContratsValide")
        public Integer nbContratsValide(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate){
            return ics.nbContratsValide(startDate,endDate);
        }


    }
