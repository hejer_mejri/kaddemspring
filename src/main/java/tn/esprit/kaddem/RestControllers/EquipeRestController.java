package tn.esprit.kaddem.RestControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.kaddem.Entities.Equipe;
import tn.esprit.kaddem.Services.IEquipeService;

import java.util.List;

@RestController
@RequestMapping("Equipe")
public class EquipeRestController {
    @Autowired
    private IEquipeService ies;

    @PostMapping("/addEquipe")
    //type de mapping
    public Equipe ajouterEquipe(@RequestBody Equipe e){
        return ies.addEquipe(e);
    }


    @PutMapping("/updateEquipe")
    public Equipe modifierEquipe(@RequestBody Equipe e){
        return ies.updateEquipe(e);}

    @GetMapping("/getEquipe")
    public List<Equipe> getAllEquipes(){
        return ies.retrieveAllEquipes();}

    @GetMapping("/getById")
    public Equipe getEquipeById(@RequestBody int id){
        return ies.retrieveEquipe(id);}



}
