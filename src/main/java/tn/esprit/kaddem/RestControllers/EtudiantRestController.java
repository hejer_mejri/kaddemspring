package tn.esprit.kaddem.RestControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.kaddem.Entities.Equipe;
import tn.esprit.kaddem.Entities.Etudiant;
import tn.esprit.kaddem.Services.EtudiantService;
import tn.esprit.kaddem.Services.IEquipeService;

import java.util.List;

@RestController
@RequestMapping("Etudiant")
public class EtudiantRestController {

    @Autowired
    private EtudiantService ie;


    @GetMapping("/getEtudiants")
    public List<Etudiant> getAllEtudiants(){
        return ie.retrieveAllEtudiants();}


    @GetMapping("/getById")
    public Etudiant getEtudiantById(@RequestBody int id){
        return ie.retrieveEtudiant(id);}


    @PutMapping("/updateEtudiant")
    public Etudiant modifierEtudiant(@RequestBody Etudiant e){
        return ie.updateEtudiant(e);}


    @PostMapping("/addEtudiant")
    public Etudiant ajouterEtudiant(@RequestBody Etudiant e){
        return ie.addEtudiant(e);
    }


    @DeleteMapping("/deleteById")
    public void deleteEtudiantById(@RequestBody int id ){ ie.removeEtudiant(id);}


    @GetMapping("/getByDepartment/{idDepartement}")
    public List<Etudiant> getEtudiantsByDepartement(@PathVariable int idDepartement){
        return ie.getEtudiantsByDepartement(idDepartement);}



    @GetMapping("/assignEtudiantToDepartement")
    public void assignEtudiantToDepartement (@RequestParam long etudiantId, @RequestParam long departementId){
        ie.assignEtudiantToDepartement(etudiantId,departementId);

    }
}
