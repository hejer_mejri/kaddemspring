package tn.esprit.kaddem.RestControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.kaddem.Entities.Department;
import tn.esprit.kaddem.Services.IDepartmentService;

import java.util.List;
@RestController
@RequestMapping("Department")

public class DepartmentRestController {
    @Autowired
    private IDepartmentService ids;

    @PostMapping("/addDepartment")
    //type de mapping
    public Department ajouterDepartment(@RequestBody Department d){
        return ids.addDepartement(d);
    }

    @PutMapping("/updateDepartment")
    public Department modifierDepartment(@RequestBody Department d){
        return ids.updateDepartement(d);}


    @GetMapping("/getDepartments")
    public List<Department> getAllDepartments(){
        return ids.retrieveAllDepartements();}

    @GetMapping("/getById")
    public Department getDepartmentById(@RequestBody int id){
        return ids.retrieveDepartement(id);}


  /*  @GetMapping("/getByUniversite")
    public List<Departement> getDepartementsByUniversite(@RequestBody int idUniversite){
        return ids.retrieveDepartementsByUniversite(idUniversite);}
*/

    @PutMapping("/assignUniversiteToDepartement")
    public void assignUniversiteToDepartement(@RequestParam Integer idUniversite, @RequestParam Integer idDepartement) {
        ids.assignUniversiteToDepartement(idUniversite, idDepartement);
    }

}

}
