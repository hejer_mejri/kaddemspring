package tn.esprit.kaddem.Repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tn.esprit.kaddem.Entities.Contrat;

import java.util.Date;
@Repository
public interface ContratRepository  extends CrudRepository<Contrat,Integer> {
    @Query(value = "select count(*) from contract  where date_finc<=:endDate "+"and date_finc>=:startDate "+"and archive=true",nativeQuery = true)

    Integer nbContratsValide(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
