package tn.esprit.kaddem.Repositories;

import org.springframework.data.repository.CrudRepository;
import tn.esprit.kaddem.Entities.DetailsEquipe;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DetailsEquipeRepository extends CrudRepository<DetailsEquipe,Long> {
    //sale, thematiques

    //select * from DetailsEquipe where salle =
    List<DetailsEquipe> findBySalle(int salle);
    //List<DetailsEquipe> getBySalle(int salle);

    //select * from DetailsEquipe where salle < ... and salle > ...
    List<DetailsEquipe> getBySalleBetween(int minSalle, int maxSalle);

    //select * from DetailsEquipe where salle < ...
    List<DetailsEquipe> getBySalleLessThan(int minSalle);


    //select * from DetailsEquipe where salle > ...
    List<DetailsEquipe> getBySalleGreaterThan(int maxSalle);


    //select * from DetailsEquipe where thematique like ...
    List<DetailsEquipe> getByThematiqueContaining(String t);

    //select * from DetailsEquipe de join Equipe e
    // on de.idDetailsEquipe = e.detailsEquipe_id_details_equipe
    // where e.niveau=
    //List<DetailsEquipe> findByEqNiveau(Niveau n);

   // List<DetailsEquipe> findByEqNiveauAndEqNomEquipeContains(Niveau n,String nom);

}

