package tn.esprit.kaddem.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.kaddem.Entities.Equipe;
@Repository

public interface EquipeRepository extends CrudRepository<Equipe,Long>{

    //SELECT
    // List<Equipe> findByNomEquipeContainingAndNiveau(String nom, Niveau n);//keyword

    // @Query("select equipe from Equipe equipe where equipe.nomEquipe=?1 and equipe.niveau=?2")
    // List<Equipe> selectByNomEtNiveauJPQL(String nom, Niveau n);//JPQL

    //@Query("select equipe from Equipe equipe where equipe.nomEquipe=:nom and equipe.niveau=:n")
    //List<Equipe> selectByNomEtNiveauJPQL2(@Param("nom") String nom, @Param("n") Niveau n);//JPQL

    //@Query(value = "select * from t_equipe where nom=?1 and n=?2 ", nativeQuery = true)
    //List<Equipe> selectByNomEtNiveauSQL(String nom, Niveau n);

    //@Query(value = "select * from t_equipe where nom=:nom and n=:n ", nativeQuery = true)
    //List<Equipe> selectByNomEtNiveauSQL2(@Param("nom") String nom, @Param("n") Niveau n);

    //Afficher la liste des equipes ==> thematique = Sport
    //  List<Equipe> findByDetailsEquipeThematique(String thematique);

    // @Query("select e from Equipe e,DetailsEquipe d where e.DetailsEquipe.idDetailsEquipe=d.idDetailsEquipe and d.thematique=?1") //OneToOne
    // List<Equipe> selectByDetailsEquipeThematiqueJPQL(String thematique);
    // @Query(value="select e from t_equipe e join details_equipe d on e.details_equipe_id_detail_equipe = d.id_detail_equipe where d.thematique=?1",nativeQuery = true) //OneToOne
    //List<Equipe> selectByDetailsEquipeThematiqueSQL(String thematique);

   /* @Modifying
    @Query("update Equipe set nomEquipe=?2 where nomEquipe like ?1 and niveau=?3")
    Equipe editEquipeByNomAndIdJPQL(String nomR,String nomN, String niveau);
    @Modifying
    @Query(value="update t_equipe set nom=?2 where nom like ?1 and n=?3", nativeQuery = true)
    Equipe editEquipeByNomAndIdSQL(String nomR,String nomN, String niveau);
/*
    @Query("SELECT equipe FROM Equipe equipe"
            + " INNER JOIN equipe.projets projet"
            + " INNER JOIN ProjetDetail detail"
            + " ON detail.idProjetDetail = projet.projetDetail.idProjetDetail"
            + " where detail.dateDebut > current_date"
            + " and detail.technologie =:technologie")
    List<Equipe> retrieveEquipesByProjetTechnologie(@Param("technologie") String technologie);

*/



}
