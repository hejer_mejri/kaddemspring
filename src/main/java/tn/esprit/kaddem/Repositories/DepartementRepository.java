package tn.esprit.kaddem.Repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.kaddem.Entities.Department;

@Repository
public interface DepartementRepository extends CrudRepository<Department, Long> {
}
