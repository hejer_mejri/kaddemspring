package tn.esprit.kaddem.Repositories;

import org.springframework.data.repository.CrudRepository;
import tn.esprit.kaddem.Entities.Etudiant;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EtudiantRepository extends CrudRepository<Etudiant,Long> {

    //select * from Etudiant where nomE="..."
    //List<Etudiant> findByNomEAndPrenomE(String nom, String prenom);

    //Etudiant getByNomEAndPrenomE(String nom, String prenom);

    //select * from Etudiant where nomE="..." and prenomE like "%...%"
    //List<Etudiant> findByNomEAndPrenomEContains(String nom, String prenom);

    //select * from Etudiant where domaine="..."
    //List<Etudiant> findByDomaine(Domaine d);

    //select * from Etudiant e join Contrat c on ....id=....id where c.dateDebutC='12/12/2025'
    //List<Etudiant> findByListContratsDateDebutC(Date date);

    //Afficher la liste des étudiants qui sont affectés à la salle(Attribut dans DetailsEquipe) 2
    //List<Etudiant> getByListEquipeDetailsEquipeSalle(int salle);

    //Afficher la liste des étudiants qui sont affectés à la salle
    //(Attribut dans DetailsEquipe) 2 et la thématique (Attribut dans DetailsEquipe)
    // "Sport"
    //List<Etudiant> getByListEquipeDetailsEquipeSalleAndListEquipeDetailsEquipeThematique(int salle, String thematique);

    //findAll
    //  @Query("select etudiant from Etudiant etudiant")
//JPQL
    //  List<Etudiant> all();

    //  @Query(value = "select * from T_Etudiant", nativeQuery = true)
//SQL
    //  List<Etudiant> all2();

    //  @Query("select etudiant from Etudiant etudiant where etudiant.prenom=:prenom and etudiant.name=:nom")
//JPQL
    //List<Etudiant> selectByNomEtPrenomJPQL(@Param("nom") String nom, @Param("prenom") String prenom);

    // @Query("select etudiant from Etudiant etudiant where etudiant.prenom=?2 and etudiant.name=?1")
//JPQL
    //List<Etudiant> selectByNomEtPrenomJPQL2(String name, String prenom);

    //@Query(value = "select * from T_Etudiant where prenom=?2 and name=?1", nativeQuery = true)
//SQL
    //List<Etudiant> selectByNomEtPrenomSQL(String name, String prenom);

    //@Query(value = "select * from T_Etudiant  where prenom=:prenom and name=:nom", nativeQuery = true)
//SQL
    // List<Etudiant> selectByNomEtPrenomSQL2(@Param("nom") String nom, @Param("prenom") String prenom);

    //Afficher la liste des étudiants selon l'état du contrat en paramètre
    //@Query(value = "select * from T_Etudiant e join contrat c on c.etudiant_id = e.id where c.archive=:a", nativeQuery = true)
//SQL
    //List<Etudiant> selectEtudiantContratSQL(@Param("a") Boolean archive);

    //@Query("select etudiant from Etudiant etudiant,Contrat contrat where contrat.e.idEtudiant= etudiant.idEtudiant" + " and contrat.archive=?1")
//JPQL
    //List<Etudiant> selectEtudiantContratJPQL(Boolean archive);

   /* @Modifying
    @Query("update  Etudiant  set prenom=?1 where name='Ferchichi'")
    Etudiant update(String prenom);
*/
    //Afficher la liste des étudiants du département "Informatique" de l'université "Esprit"

   /* @Query(value = " SELECT * from t_etudiant e " +
            "JOIN departement d on e.departement_id_departement=d.id_departement " +
            "JOIN universite_list_departements asso on asso.list_departements_id_departement=d.id_departement " +
            "JOIN universite u on u.id_universite= asso.universite_id_universite " +
            "where u.nom_universite=?2 and d.nom_departement=?1",
            nativeQuery = true)
    List<Etudiant> select(String departement, String universite);

    @Query("select e from Etudiant e, Departement d where e.d.idDepartement" +
            "=d.idDepartement and d.idDepartement=?1")
    List<Etudiant> selectEtudiantByDepartementJPQL(long departmentId);

    @Query(value="select e from t_etudiant e join departement d on e.departement_id_departement=d.id_departement" +
            " where d.id_departement=:d",nativeQuery = true)
    List<Etudiant> selectEtudiantByDepartementSQL(@Param ("d") long departmentId);

*/





    //Afficher la liste des étudiants de l'université dont son nom est passé en paramètre

    //KEYWORD
    // List<Etudiant> findByDepartement ==> Impossible

    //JPQL
    /*@Query("select e from Etudiant e join Departement d on e.departement.idDepartement=d.idDepartement" +
            " join Universite u on u.listDepartements=d.idDepartement")
    List<Etudiant> recuperer(String nom);*/

    //SQL
  /*  @Query(value = "select e from t_etudiant e " +
            "join  departement d on d.id_departement=e.departement_id_departement " +
            "join t_universite_list_departements ud on ud.list_departements_id_departement= d.id_departement" +
            "join t_universite u on ud.universite_id_universite=u.id_universite " +
            "where u.nom_universite=?1",nativeQuery = true)
    List<Etudiant> recuperer(String nom);
*/

    //Afficher la liste des étudiants dont la spécialité de son contrat est passé en paramètre
    //JPQL

   /* @Query("select e from Etudiant e join Contrat c on " +
            "c.e.idEtudiant=e.idEtudiant where c.specialite=:s")
    List<Etudiant> recuperer2(@Param("s") Specialite spec);*/




    Etudiant findByNameAndPrenom(String nomEtudiant, String prenomEtudiant);



    List<Etudiant> getEtudiantByDIdDepartement(int idDepartment);

    @Query("select e from Etudiant e , Departement  d where d.idDepartement=?1")
    List <Etudiant> selectEtudiantByDIdDepartmentJPQL(int idDepartment);

    @Query(value = "select e from etudiant e,departement d where d.id_departement=?1",nativeQuery = true)
    List<Etudiant> selectEtudiantByDIdDepartementSQL(int idDepartment);



    //
}
